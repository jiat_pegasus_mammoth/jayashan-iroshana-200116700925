package com.jayashan.web;

import com.jayashan.web.db.MySQL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(name = "Update", urlPatterns = "/Update")
public class Update extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        String currentemail = session.getAttribute("user").toString();

        String fname = req.getParameter("fname");
        String lname = req.getParameter("lname");

        if(fname.isEmpty()){
            resp.getWriter().write("Enter First Name");
        }else if(lname.isEmpty()){
            resp.getWriter().write("Enter Last Name");
        } else {

            if(currentemail.equals("admin@gmail.com")){
                resp.getWriter().write("You cannot used admin email");
            }else{
                try {
                    ResultSet resultset = MySQL.search("SELECT * FROM `user` WHERE `email` = '" + currentemail + "' AND `status_id` = '1'");
                    if(resultset.next()){
                        MySQL.iud("UPDATE `user` SET `fname` = '"+fname+"', `lname` = '"+lname+"' WHERE `email` = '"+currentemail+"'");
                        resp.getWriter().write("Success");
                    }else{
                        resp.getWriter().write("Denied");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
