package com.jayashan.web;

import com.jayashan.web.db.MySQL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.ResultSet;

@WebServlet(name = "Remove", urlPatterns = "/Remove")
public class Remove extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String email = req.getParameter("email");
        String status = req.getParameter("status");

        try {
            ResultSet resultset = MySQL.search("SELECT * FROM `user` WHERE `email` = '" + email + "'");
            if (resultset.next()) {

                if(status.equals("Active")){
                    MySQL.iud("UPDATE `user` SET `status_id` = '2' WHERE `email` = '"+email+"'");
                }else{
                    MySQL.iud("UPDATE `user` SET `status_id` = '1' WHERE `email` = '"+email+"'");
                }
                resp.getWriter().write("Success");
            } else {
                resp.getWriter().write("User not found");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}