package com.jayashan.web;

import com.jayashan.web.db.MySQL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(name = "SignUp", urlPatterns = "/SignUp")
public class SignUp extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String fname = req.getParameter("fname");
        String lname = req.getParameter("lname");
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        if(fname.isEmpty()){
            resp.getWriter().write("Enter First Name");
        }else if(lname.isEmpty()){
            resp.getWriter().write("Enter Last Name");
        }else if (email.isEmpty()) {
            resp.getWriter().write("Enter Email");
        } else if (password.isEmpty()) {
            resp.getWriter().write("Enter Password");
        } else {

            if(email.equals("admin@gmail.com") && password.equals("1234")){
                resp.getWriter().write("You cannot used admin email and password");
            }else{
                try {
                    ResultSet resultset = MySQL.search("SELECT * FROM `user` WHERE `email` = '" + email + "'");
                    if(resultset.next()){
                        resp.getWriter().write("User already exist");
                    }else{
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = new Date();
                        String today = formatter.format(date);
                        MySQL.iud("INSERT INTO `user` (`fname`,`lname`,`email`,`password`,`register_date`,`status_id`) VALUES ('"+fname+"','"+lname+"','"+email+"','"+password+"','"+today+"','1')");
                        resp.getWriter().write("Success");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
