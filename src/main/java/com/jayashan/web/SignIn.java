package com.jayashan.web;

import com.jayashan.web.db.MySQL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.ResultSet;

@WebServlet(name = "SignIn", urlPatterns = "/SignIn")
public class SignIn extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String email = req.getParameter("email");
        String password = req.getParameter("password");

        if (email.isEmpty()) {
            resp.getWriter().write("Enter Email");
        } else if (password.isEmpty()) {
            resp.getWriter().write("Enter Password");
        } else {

            HttpSession session = req.getSession();

            if (email.equals("admin@gmail.com") && password.equals("1234")) {
                resp.getWriter().write("Admin");
                session.setAttribute("admin", "admin");
            } else {

                try {
                    ResultSet resultset = MySQL.search("SELECT * FROM `user` WHERE `email` = '" + email + "' AND `password` = '" + password + "' AND `status_id`='1'");
                    if (resultset.next()) {
                        session.setAttribute("user", email);
                        resp.getWriter().write("Success");
                    } else {
                        resp.getWriter().write("Invalid details");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
