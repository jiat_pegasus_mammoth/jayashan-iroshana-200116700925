package com.jayashan.web.db;

import com.jayashan.web.util.ApplicationProperties;

import java.sql.*;

public class MySQL {
    private static Connection connection;

    private static Statement createConnection() throws Exception {
        ApplicationProperties properties = ApplicationProperties.getInstance();
        Class.forName(properties.get("sql.connection.driver"));
        connection = DriverManager.getConnection(properties.get("sql.connection.url"), properties.get("sql.connection.user"), properties.get("sql.connection.password"));
        return connection.createStatement();
    }

    public static void iud(String query) {
        try {
            createConnection().executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try{
                connection.close();
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
    }

    public static ResultSet search(String query) throws Exception {
        return createConnection().executeQuery(query);
    }

    public static Connection getConnection() throws Exception {
        ApplicationProperties properties = ApplicationProperties.getInstance();
        Class.forName(properties.get("sql.connection.driver"));
        connection = DriverManager.getConnection(properties.get("sql.connection.url"), properties.get("sql.connection.user"), properties.get("sql.connection.password"));
        return connection;
    }
}
