<%--
  Created by IntelliJ IDEA.
  User: jayas
  Date: 29/03/2023
  Time: 11:00
  To change this template use File | Settings | File Templates.
--%><%
    String email;
    if (session.getAttribute("user") != null) {
        response.sendRedirect("home.jsp");
        return;
    }

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>WebProgramming_2_HomeWork_01 | Sign In - Sign Up</title>
    <link rel="stylesheet" href="css/all.min.css"/>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>
</head>
<body>

<div class="container-fluid vh-100 p-5">
    <div class="row mt-lg-5">
        <!--signin-->
        <div class="col-12 col-lg-6 offset-lg-3 bg-white shadow p-3" id="signinform">
            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="fw-bold">Sign In</h1>
                </div>
                <div class="text-danger mt-4 small" id="msg1"></div>
                <div class="col-12 d-grid mt-4">
                    <label class="form-label"><i class="fas fa-envelope"></i>&nbsp;&nbsp;Email</label>
                    <input class="form-control" placeholder="Email" type="email" id="email"/>
                </div>
                <div class="col-12 d-grid mt-4">
                    <label class="form-label"><i class="fas fa-lock"></i>&nbsp;&nbsp;Password</label>
                    <input class="form-control" placeholder="Password" type="password" id="password"/>
                </div>
                <div class="col-12 col-lg-6 offset-lg-3 d-grid mt-4">
                    <button class="btn btn-primary" onclick="signIn();">Sign In</button>
                </div>
                <div class="col-12 d-grid mt-3">
                    <label class="form-label" onclick="changeForm();">Don't have an account? <a class="link-primary"
                                                                                                href="#">Sign
                        up</a></label>
                </div>
                <div class="col-12 d-grid mt-3">
                    <label class="form-label">Admin Email : admin@gmail.com</label>
                    <label class="form-label">Admin Password : 1234</label>
                </div>
            </div>
        </div>
        <!--signin-->

        <!--signup-->
        <div class="col-12 col-lg-6 offset-lg-3 bg-white shadow d-none p-3" id="signupform">
            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="fw-bold">Sign Up</h1>
                </div>
                <div class="text-danger mt-4 small" id="msg2"></div>
                <div class="col-6 d-grid mt-4">
                    <label class="form-label"><i class="fas fa-user"></i>&nbsp;&nbsp;First Name</label>
                    <input class="form-control" placeholder="First Name" type="text" id="fname"/>
                </div>
                <div class="col-6 d-grid mt-4">
                    <label class="form-label"><i class="fas fa-user"></i>&nbsp;&nbsp;Last Name</label>
                    <input class="form-control" placeholder="Last Name" type="text" id="lname"/>
                </div>
                <div class="col-12 d-grid mt-4">
                    <label class="form-label"><i class="fas fa-envelope"></i>&nbsp;&nbsp;Email</label>
                    <input class="form-control" placeholder="Email" type="email" id="email2"/>
                </div>
                <div class="col-12 d-grid mt-4">
                    <label class="form-label"><i class="fas fa-lock"></i>&nbsp;&nbsp;Password</label>
                    <input class="form-control" placeholder="Password" type="password" id="password2"/>
                </div>
                <div class="col-12 col-lg-6 offset-lg-3 d-grid mt-4">
                    <button class="btn btn-primary" onclick="signUp();">Sign Up</button>
                </div>
                <div class="col-12 d-grid mt-3">
                    <label class="form-label" onclick="changeForm();">Already have an account? <a class="link-primary"
                                                                                                  href="#">Sign
                        in</a></label>
                </div>
            </div>
        </div>
        <!--signup-->
    </div>
</div>

<script src="js/all.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/script.js"></script>

</body>
</html>
