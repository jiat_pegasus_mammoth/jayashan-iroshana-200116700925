<%@ page import="java.sql.ResultSet" %>
<%@ page import="com.jayashan.web.db.MySQL" %><%--
  Created by IntelliJ IDEA.
  User: jayas
  Date: 31/03/2023
  Time: 13:40
  To change this template use File | Settings | File Templates.
--%>

<%
    String email = null;
    String fname = null;
    String lname = null;
    String registerdate = null;

    if (session.getAttribute("user") == null) {
        response.sendRedirect("index.jsp");
        return;
    } else {
        email = session.getAttribute("user").toString();
        try {
            ResultSet resultset = MySQL.search("SELECT * FROM `user` WHERE `email` = '" + email + "'");
            if (resultset.next()) {
                fname = resultset.getString("fname");
                lname = resultset.getString("lname");
                registerdate = resultset.getString("register_date");
            } else {
                response.sendRedirect("index.jsp");
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>WebProgramming_2_HomeWork_01 | Home</title>
    <link rel="stylesheet" href="css/all.min.css"/>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>
</head>
<body>

<div class="container-fluid p-5 d-flex justify-content-center align-items-center vh-100">
    <div class="row mt-lg-5">
        <div class="col-12 bg-white shadow p-4">
            <div class="row">
                <div class="col-12 d-grid flex flex-column gap-3">
                    <div>
                        <label class="fw-bold fs-5">Name</label>
                        <label class="fw-bold fs-5">:</label>
                        <label><%=fname + " " + lname%>
                        </label>
                    </div>
                    <div>
                        <label class="fw-bold fs-5">Email</label>
                        <label class="fw-bold fs-5">:</label>
                        <label><%=email%></label>
                    </div>
                    <div>
                        <label class="fw-bold fs-5">Registered Date</label>
                        <label class="fw-bold fs-5">:</label>
                        <label><%=registerdate%> </label>
                    </div>
                    <div class="row flex justify-content-between flex-row gap-3">
                        <button class="btn btn-primary" onclick="openUpdateProfileModal();">Update Profile</button>
                        <a href="signout.jsp" class="btn btn-danger" style="cursor: pointer">Sign Out</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="updateProfileModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Profile Details</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-6 d-grid mt-4">
                        <label class="form-label"><i class="fas fa-user"></i>&nbsp;&nbsp;First Name</label>
                        <input id="fname" class="form-control" placeholder="First Name" type="text" value=<%=fname%> />
                    </div>
                    <div class="col-6 d-grid mt-4">
                        <label class="form-label"><i class="fas fa-user"></i>&nbsp;&nbsp;Last Name</label>
                        <input id="lname" class="form-control" placeholder="Last Name" type="text" value=<%=lname%> />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="updateProfile();">Update</button>
            </div>
        </div>
    </div>
</div>

<script src="js/all.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/script.js"></script>

</body>
</html>
