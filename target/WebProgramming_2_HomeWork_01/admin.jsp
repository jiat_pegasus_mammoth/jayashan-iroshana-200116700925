<%@ page import="java.sql.ResultSet" %>
<%@ page import="com.jayashan.web.db.MySQL" %><%--
  Created by IntelliJ IDEA.
  User: jayas
  Date: 31/03/2023
  Time: 15:03
  To change this template use File | Settings | File Templates.
--%>

<%
    String admin = null;

    if (session.getAttribute("admin") == null) {
        response.sendRedirect("index.jsp");
        return;
    } else {
        admin = session.getAttribute("admin").toString();
    }

%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>WebProgramming_2_HomeWork_01 | Admin</title>
    <link rel="stylesheet" href="css/all.min.css"/>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>
</head>
<body>

<div class="container-fluid p-5">

    <div class="row mb-5">
        <div class="col-12 text-center fw-bold fs-3">
            <label class="form-label">All Users</label>
        </div>
    </div>

    <table class="table table-success table-striped table-bordered">
        <thead>
        <tr>
            <th class="text-center" scope="col">Id</th>
            <th class="text-center" scope="col">First Name</th>
            <th class="text-center" scope="col">Last Name</th>
            <th class="text-center" scope="col">Email</th>
            <th class="text-center" scope="col">Registered Date</th>
            <th class="text-center" scope="col">Status</th>
            <th class="text-center" scope="col">Action</th>
        </tr>
        </thead>
        <tbody>

        <%

            try {
                ResultSet resultset = MySQL.search("SELECT * FROM `user` INNER JOIN `status` ON `user`.`status_id` = `status`.`id`");
                while (resultset.next()) {
                    String email = resultset.getString("email");
                    String status = resultset.getString("name");
        %>
        <tr>
            <td class="text-center"><%=resultset.getString("id")%>
            </td>
            <td class="text-center"><%=resultset.getString("fname")%>
            </td>
            <td class="text-center"><%=resultset.getString("lname")%>
            </td>
            <td class="text-center"><%=email%>
            </td>
            <td class="text-center"><%=resultset.getString("register_date")%>
            </td>
            <td class="text-center"><%=status%>
            </td>
            <td class="text-center">
                <%
                    if (resultset.getString("name").equals("Active")) {
                %>
                <button class="btn btn-danger btn-sm"
                        onclick="removeUser('<%=email%>','<%=status%>')">Remove
                </button>
                <%
                } else {
                %>
                <button class="btn btn-success btn-sm"
                        onclick="removeUser('<%=email%>','<%=status%>')">Add
                </button>
                <%
                    }
                %>
            </td>
        </tr>
        <%
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        %>

        </tbody>
    </table>
</div>

<script src="js/all.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/script.js"></script>
</html>
