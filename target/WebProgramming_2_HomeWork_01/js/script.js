function changeForm() {
    var signupform = document.getElementById("signupform");
    var signinform = document.getElementById("signinform");

    signupform.classList.toggle("d-none");
    signinform.classList.toggle("d-none");
}

function signUp() {
    var fname = document.getElementById("fname");
    var lname = document.getElementById("lname");
    var email = document.getElementById("email2");
    var password = document.getElementById("password2");
    var msg = document.getElementById("msg2");

    var r = new XMLHttpRequest();

    r.onreadystatechange = function() {

        if (r.readyState == 4) {
            var t = r.responseText;
            if (t == "Success") {
                fname.value = "";
                lname.value = "";
                email.value = "";
                password.value = "";
                msg.innerHTML = "";
                changeForm();
            } else {
                msg.innerHTML = t;
            }
        }
    };
    r.open("POST", "SignUp", true);
    r.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    r.send("fname=" + fname.value + "&lname=" + lname.value+"&email=" + email.value + "&password=" + password.value);
}

function signIn() {
    var email = document.getElementById("email");
    var password = document.getElementById("password");
    var msg1 = document.getElementById("msg1")

    var r = new XMLHttpRequest();

    r.onreadystatechange = function () {
        if (r.readyState == 4) {
            var t = r.responseText;
            if (t == "Success") {
                msg1.innerHTML = "";
                email.value ="";
                password.value ="";
                window.location = "home.jsp";
            }else if(t=="Admin"){
                email.value ="";
                password.value ="";
                window.location = "admin.jsp";
            } else {
                msg1.innerHTML = t;
            }
        }
    };
    r.open("POST", "SignIn", true);
    r.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    r.send("email=" + email.value + "&password=" + password.value);
}

var modal;

function openUpdateProfileModal() {
    let updateProfileModal = document.getElementById("updateProfileModal");
    modal = new bootstrap.Modal(updateProfileModal);
    modal.show();
}

function updateProfile(){
    var fname = document.getElementById("fname");
    var lname = document.getElementById("lname");

    var r = new XMLHttpRequest();

    r.onreadystatechange = function() {

        if (r.readyState == 4) {
            var t = r.responseText;
            if (t == "Success") {
                fname.value = "";
                lname.value = "";
                window.location = 'home.jsp';
            } else {
                alert("You cannot update. Please contact system admin for more details.");
            }
        }
    };
    r.open("POST", "Update", true);
    r.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    r.send("fname=" + fname.value + "&lname=" + lname.value);
}

function removeUser(email,status){
    var r = new XMLHttpRequest();

    r.onreadystatechange = function() {

        if (r.readyState == 4) {
            var t = r.responseText;
            if (t == "Success") {
                window.location = 'admin.jsp';
            } else {
                alert(t);
            }
        }
    };
    r.open("POST", "Remove", true);
    r.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    r.send("email=" + email + "&status=" + status);
}